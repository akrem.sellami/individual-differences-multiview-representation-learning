**Presentation of the toolbox**

This is toolbox has been designed for mapping individual differences using mutli-view representation learning.

**Installation**

See INSTALL.md for more information.

**Note**

We used the  source code of "convexminimization.py" developed by François-Xavier Dupé (https://gitlab.lis-lab.fr/francois-xavier.dupe)

